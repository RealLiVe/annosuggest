package com.mycompany.annosuggest.parser;

import com.mycompany.annosuggest.parser.path.PathFragment;
import com.mycompany.annosuggest.parser.path.ReverseFragment;
import com.mycompany.annosuggest.parser.test.IsATestFragment;
import org.junit.Test;

/**
 * Created by RealLiVe on 08.03.2016.
 */
public class PathParseTest extends AbstractParserTest {
    Fragment v;

    @Test
    public void testEmptyPath() throws Exception {
        v = testSimpleExpression("", "", PathFragment.class, "");
        v = testSimpleExpression("", " ", PathFragment.class, "");
        v = testSimpleExpression("", "   ", PathFragment.class, "");
    }

    @Test
    public void testSimplePath() throws Exception {
        v = testSimpleExpression("", "foaf:kno", UnknownValueFragment.class, "");
        v = testSimpleExpression("", "foaf:knows/", KeyFragment.class, "foaf:knows");
        v = testSimpleExpression("", "foaf:knows/foa", UnknownValueFragment.class, "foaf:knows");
        v = testSimpleExpression("", "<http://google.", URIFragment.class, "");
        v = testSimpleExpression("", "<http://google.de>", URIFragment.class, "");
        v = testSimpleExpression("", "<http://google.de>/foaf", UnknownValueFragment.class, "<http://google.de>");
    }

    @Test
    public void reversePathTest() throws Exception {
        v = testSimpleExpression("", "^foaf", ReverseFragment.class, "");
        v = testSimpleExpression("", "^<http://example.org>", ReverseFragment.class, "");
        v = testSimpleExpression("", "^<http://example.org>/foaf", UnknownValueFragment.class, "^<http://example.org>");
    }

    @Test
    public void groupPathTest() throws Exception {
        testSimpleExpression("", "(foaf:knows & foaf:example", UnknownValueFragment.class, "");
        testSimpleExpression("", "(foaf:knows & foaf:example/", KeyFragment.class, "foaf:example");
        testSimpleExpression("", "(foaf:knows | foaf:example)/foaf:knows", UnknownValueFragment.class, "(foaf:knows | foaf:example)");
        testSimpleExpression("", "([is-a foaf", IsATestFragment.class, "");
    }

    @Test
    public void complexPathTest() throws Exception {
        v = testSimpleExpression("", "foaf:knows", UnknownValueFragment.class, "");
        v = testSimpleExpression("", "foaf:knows/", KeyFragment.class, "foaf:knows");
        v = testSimpleExpression("", "foaf:knows/<uri", URIFragment.class, "foaf:knows");
        v = testSimpleExpression("", "foaf:knows/<uri>/(", PathFragment.class, "foaf:knows/<uri>");
        v = testSimpleExpression("", "foaf:knows/<uri>/(^oa:hasBody | oa:hasBody)", ValueFragment.class, "foaf:knows/<uri>");
    }

    @Test(expected = ParseException.class)
    public void testInvalidUnknownAfterGroup() throws Exception {
        v = testSimpleExpression("", "foaf:knows/<uri>/(^oa:hasBody | oa:hasBody)foaf"
                , UnknownValueFragment.class, "foaf:knows/<uri>/(^oa:hasBody | oa:hasBody)");
    }
}
