package com.mycompany.annosuggest.parser;

import java.util.LinkedList;
import java.util.logging.Logger;

import static org.junit.Assert.assertTrue;

/**
 * Created by RealLiVe on 08.03.2016.
 */
public abstract class AbstractParserTest {
    Logger log = Logger.getLogger(PathParseTest.class.getName());
    Parser parser = new Parser();

    protected void testSimpleExpression(String message, String ldPath, Class<?> instance) throws Exception {
        this.testSimpleExpression(message, ldPath, instance, null);
    }

    protected Fragment testSimpleExpression(String message, String ldPath, Class<?> instance, String generatedPath) throws Exception {
        log.info("# Start Test  : " + message + "");
        log.info("# Given String: " + ldPath + "");

        Fragment x = parser.parse(ldPath);

        LinkedList<Fragment> track = new LinkedList<>();
        String path = x.generateExpression(track);

        log.info("# Calculated: "
                + path.trim());
        if (generatedPath != null) {
            assertTrue(generatedPath.trim().equals(path.trim()));
        }
        log.info("# List-Size: "
                + track.size());
        assertTrue(message + ": Tracking", !track.isEmpty());
        log.info("# Class: " + track.getFirst().getClass().getName());

        if (instance != null) {
            log.info("# Requested Class: " + instance.isAssignableFrom(track.getFirst().getClass()));
            assertTrue(message + ": Tracking Class", instance.isAssignableFrom(track.getFirst().getClass()));
        }
        log.info("# End Test  : " + message + "");

        return x;
    }

    protected Fragment getLeafTrack(Fragment x) throws Exception {
        LinkedList<Fragment> l = new LinkedList<>();
        x.generateExpression(l);
        if (l.isEmpty()) {
            throw new Exception();
        }
        return l.getLast();
    }
}
