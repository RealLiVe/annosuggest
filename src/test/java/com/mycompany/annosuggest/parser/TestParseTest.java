package com.mycompany.annosuggest.parser;

import com.mycompany.annosuggest.parser.test.IsATestFragment;
import com.mycompany.annosuggest.parser.test.IsTestFragment;
import com.mycompany.annosuggest.parser.test.LangTestFragment;
import com.mycompany.annosuggest.parser.test.UnknownTestValueFragment;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by RealLiVe on 08.03.2016.
 */
public class TestParseTest extends AbstractParserTest {
    Fragment x;

    @Test
    public void simpleTest() throws Exception {
        x = testSimpleExpression("", "foaf:knows[fo", UnknownTestValueFragment.class, "foaf:knows");
    }

    @Test
    public void langTestParse() throws Exception {
        testSimpleExpression("", "[@d", LangTestFragment.class, "");
        testSimpleExpression("", "foaf:example[@d", LangTestFragment.class, "foaf:example");
        x = getLeafTrack(testSimpleExpression("", "[@de & @e", LangTestFragment.class, ""));
        assertTrue(x instanceof LangTestFragment);
        x = x.getFragment().getLast();
        assertTrue(x instanceof UnknownTestValueFragment);
        assertTrue(((UnknownTestValueFragment) x).getValue().trim().equals("e"));
    }

    @Test
    public void isATestParse() throws Exception {
        x = testSimpleExpression("", "[is-a<", IsATestFragment.class, "");
        x = testSimpleExpression("", "[is-a foaf:test & is-a<uri1> &is-a <uri2>", IsATestFragment.class, "");
        x = getLeafTrack(x);
        assertTrue(x instanceof IsATestFragment);
        assertTrue(x.generateFullExpression().trim().equals("is-a <uri2>"));
    }

    @Test
    public void existenceParseTest() throws Exception {
        x = testSimpleExpression("", "[ex:is & oa:", null, "");
        x = testSimpleExpression("", "[ex:is]/oa:", UnknownValueFragment.class, "[ex:is]");
        x = testSimpleExpression("", "[ex:is & test is \"one\"]/oa:", UnknownValueFragment.class, "[ex:is & test is \"one\"]");
        x = testSimpleExpression("", "[<uri>]/oa:", UnknownValueFragment.class, "[<uri>]");
        x = testSimpleExpression("", "[<uri> i", IsTestFragment.class, "");
    }
    @Test
    public void isTestParse() throws Exception {
        x = testSimpleExpression("", "[ex:test", null, "");
        x = testSimpleExpression("", "[ex:test is", IsTestFragment.class, "");
        x = testSimpleExpression("", "[ex:test i", IsTestFragment.class, "");
        x = testSimpleExpression("", "[ex:test is ", IsTestFragment.class, "");
        x = testSimpleExpression("", "[ex:test is foaf:test", IsTestFragment.class, "");
        x = testSimpleExpression("", "[<uri>is foaf:test", IsTestFragment.class, "");
        x = testSimpleExpression("", "[<uri>is<uri>", IsTestFragment.class, "");
        x = testSimpleExpression("", "[<uri> is  <uri>", IsTestFragment.class, "");
        IsTestFragment y = (IsTestFragment) getLeafTrack(x);
        assertTrue(y.generateFullExpression().equals("<uri> is <uri>"));
    }


}
