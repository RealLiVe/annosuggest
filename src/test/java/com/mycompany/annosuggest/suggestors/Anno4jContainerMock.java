package com.mycompany.annosuggest.suggestors;

import com.github.anno4j.Anno4j;
import com.github.anno4j.querying.QueryService;

/**
 * Created by RealLiVe on 15.03.2016.
 */
public class Anno4jContainerMock extends Anno4jContainer {
    private Anno4j anno4j;

    public Anno4jContainerMock(Anno4j anno4j) {
        this.anno4j = anno4j;
    }

    public QueryService createQueryService() {
        QueryService q = anno4j.createQueryService().addPrefix("ex", "http://www.example.com/schema#");
        return q;
    }
}
