package com.mycompany.annosuggest.suggestors.pathpredict;

import com.github.anno4j.Anno4j;
import com.github.anno4j.model.Annotation;
import com.github.anno4j.model.Body;
import com.github.anno4j.model.Target;
import com.mycompany.annosuggest.io.InputDTO;
import com.mycompany.annosuggest.io.ResultDTO;
import com.mycompany.annosuggest.suggestors.Anno4jContainerMock;
import com.mycompany.annosuggest.suggestors.exception.InputException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.annotations.Iri;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.LangString;

import java.util.LinkedList;
import java.util.Locale;

import static org.junit.Assert.assertTrue;

/**
 * Created by RealLiVe on 15.03.2016.
 */
public class PathPredictTest {

    static PathPredict predictor;
    static Anno4j anno4j;

    @BeforeClass
    public static void init() throws Exception {
        anno4j = new Anno4j();
        persistTestData();
        predictor = new PathPredict();
        predictor.setAnno4jContainer(new Anno4jContainerMock(anno4j));
    }

    @Test
    public void testSimplePathExpressions() throws Exception {
        process("<", new String[]{
                "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
                "http://www.w3.org/ns/oa#hasBody",
                "http://www.w3.org/ns/oa#hasTarget",
                "http://www.w3.org/ns/oa#serializedAt"});

        process("oa:", new String[]{
                "oa:hasBody",
                "oa:hasTarget",
                "oa:serializedAt"});

        process("oa:hasB", new String[]{
                "oa:hasBody"});
    }

    @Test
    public void testTypeTest() throws Exception {
        process("oa:hasBody[is-a ex:", new String[]{
                "ex:bananaBody",
                "ex:mangoBody",
                "ex:appleBody"});

        process("oa:hasBody[is-a ex:b", new String[]{
                "ex:bananaBody"});

        process("oa:hasBody[is-a <", new String[]{
                "http://www.example.com/schema#bananaBody",
                "http://www.example.com/schema#mangoBody",
                "http://www.example.com/schema#appleBody"});

        process("oa:hasBody[is-a <h", new String[]{
                "http://www.example.com/schema#bananaBody",
                "http://www.example.com/schema#mangoBody",
                "http://www.example.com/schema#appleBody"});

        process("oa:hasBody[is-a <http://www.example.com/schema#m", new String[]{
                "http://www.example.com/schema#mangoBody"});

    }

    @Test
    public void testLangTest() throws Exception {
        process("oa:hasBody/ex:mangoValue[@", new String[]{
                "de",
                "en"});
    }

    @Test
    public void testReversePath() throws Exception {
        process("oa:hasBody/^o", new String[]{
                "oa:hasBody"});
        process("oa:hasBody/^<h", new String[]{
                "http://www.w3.org/ns/oa#hasBody"});
    }

    @Test
    public void testTest() throws Exception {
        // only one literal at annotation level saved.
        process("[", new String[]{
                "oa:serializedAt"});
    }
    @Test
    public void testIsTest() throws Exception {
        process("oa:hasBody[<http://www.example.com/schema#appleVal", new String[]{
                "http://www.example.com/schema#appleValue"});

        process("oa:hasBody[<http://www.example.com/schema#appleValue> i", new String[]{
                "is"});
        process("oa:hasBody[<http://www.example.com/schema#appleValue> is \"", new String[]{
                "4"});

        process("oa:hasBody[ex:appl", new String[]{
                "ex:appleValue"});
        process("oa:hasBody[ex:appleValue is \"", new String[]{
                "4"});

        process("oa:hasBody[ex:bananaVal", new String[]{
                "ex:bananaValue"});

        process("oa:hasBody[ex:bananaValue is \"I am", new String[]{
                "I am a Banana!"});
    }

    @Test
    public void testCombination() throws Exception {
        process("oa:hasBody[ex:appleValue is \"4\"]/^oa:", new String[]{
                "oa:hasBody"});
    }


    @Test(expected = InputException.class)
    public void testTestIllegalDelimiter() throws Exception {
        process("oa:hasBody[[", new String[]{
                "oa:hasBody"});
    }

    @Test
    public void testInput() throws Exception {
        process("", new String[]{"rdf:type", "oa:hasBody", "oa:hasTarget", "oa:serializedAt"});
        process("o", new String[]{"oa:hasBody", "oa:hasTarget", "oa:serializedAt"});
        process("oa", new String[]{"oa:hasBody", "oa:hasTarget", "oa:serializedAt"});
        process("oa:", new String[]{"oa:hasBody", "oa:hasTarget", "oa:serializedAt"});
        process("oa:h", new String[]{"oa:hasBody", "oa:hasTarget"});
        process("oa:hasB", new String[]{"oa:hasBody"});
        process("oa:hasBody", new String[]{"oa:hasBody"});
        process("oa:hasBody/", new String[]{"rdf:type", "ex:bananaValue", "ex:mangoValue", "ex:appleValue"});
        process("oa:hasBody/^", new String[]{"oa:hasBody"});
        process("oa:hasBody/^<", new String[]{"http://www.w3.org/ns/oa#hasBody"});
        process("oa:hasBody/^<http://www.w3.org/ns/oa#hasBody", new String[]{"http://www.w3.org/ns/oa#hasBody"});
        process("oa:hasBody/^<http://www.w3.org/ns/oa#hasBody>/(", new String[]{"rdf:type", "oa:hasBody", "oa:hasTarget", "oa:serializedAt"});
    }
    protected void process(String ldpath, String[] results) throws Exception {
        ResultDTO d = predictor.suggest(new InputDTO(ldpath));
        LinkedList<String> res = d.getSuggestion();
        assertTrue("size different", res.size() == results.length);
        for (int i = 0; i < results.length; i++) {
            String left = results[i];
            String right = res.get(i);
            assertTrue("order failure", left.equals(right));
        }
    }

    private static void persistTestData() throws RepositoryException, InstantiationException, IllegalAccessException {
        Annotation annotation = anno4j.createObject(Annotation.class);
        annotation.setSerializedAt("07.05.2015");
        BananaBody body1 = anno4j.createObject(BananaBody.class);
        body1.setBananaValue("I am a Banana!");
        ConstraintLessTarget target = anno4j.createObject(ConstraintLessTarget.class);
        annotation.addTarget(target);
        annotation.setBody(body1);
        anno4j.persist(annotation);

        Annotation annotation2 = anno4j.createObject(Annotation.class);
        annotation2.setSerializedAt("17.05.2015");
        annotation2.setBody(body1);
        anno4j.persist(annotation2);

        Annotation annotation3 = anno4j.createObject(Annotation.class);
        annotation3.setSerializedAt("30.01.2016");
        AppleBody body2 = anno4j.createObject(AppleBody.class);
        body2.setAppleValue("4");
        annotation3.setBody(body2);
        anno4j.persist(annotation3);

        Annotation annotation4 = anno4j.createObject(Annotation.class);
        MangoBody body3 = anno4j.createObject(MangoBody.class);
        body3.setMangoValue(new LangString("Hallo", Locale.GERMAN));
        annotation4.setBody(body3);
        anno4j.persist(annotation4);

        Annotation annotation5 = anno4j.createObject(Annotation.class);
        MangoBody body4 = anno4j.createObject(MangoBody.class);
        body4.setMangoValue(new LangString("Hello", Locale.ENGLISH));
        annotation5.setBody(body4);
        anno4j.persist(annotation5);
    }

    @Iri("http://www.example.com/schema#appleBody")
    public static interface AppleBody extends Body {

        @Iri("http://www.example.com/schema#appleValue")
        public String getAppleValue();

        @Iri("http://www.example.com/schema#appleValue")
        public void setAppleValue(String value);
    }

    @Iri("http://www.example.com/schema#mangoBody")
    public static interface MangoBody extends Body {

        @Iri("http://www.example.com/schema#mangoValue")
        public LangString getMangoValue();

        @Iri("http://www.example.com/schema#mangoValue")
        public void setMangoValue(LangString value);
    }

    @Iri("http://www.example.com/schema#bananaBody")
    public static interface BananaBody extends Body {

        @Iri("http://www.example.com/schema#bananaValue")
        public String getBananaValue();

        @Iri("http://www.example.com/schema#bananaValue")
        public void setBananaValue(String value);
    }

    @Iri("http://www.example.com/schema#constraintLessTarget")
    public static interface ConstraintLessTarget extends Target {

        @Iri("http://www.example.com/schema#annoval")
        public Annotation getValue();

        @Iri("http://www.example.com/schema#annoval")
        public void setValue(Annotation value);
    }

}