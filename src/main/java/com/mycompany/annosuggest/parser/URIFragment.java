package com.mycompany.annosuggest.parser;

import com.mycompany.annosuggest.parser.interfaces.LeafFragment;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Contains an URI.
 */
public class URIFragment extends Fragment implements LeafFragment {

    String value = "";

    @Override
    public Fragment parse(StringReader in) throws IOException {
        // Breakpoints
        Pattern tokens = Pattern.compile((
                Pattern.quote(">"))
        );

        String ldPath = this.readUntilEnd(in);
        Matcher matcher = tokens.matcher(ldPath);

        if (matcher.find()) {
            String delimiter = ldPath.substring(matcher.start(), matcher.end());
            String fragStr = ldPath.substring(0, matcher.start());

            // refresh matcher
            in.skip(matcher.end());

            switch (delimiter) {
                case ">":
                    this.addFragment(new ValueFragment(fragStr));
                    this.addFragment(new KeyFragment(">"));
                    return this;
            }
        } else {
            in.skip(ldPath.length());
            this.addFragment(new ValueFragment(ldPath));
        }
        return this;
    }

    @Override
    public String generateExpression(List<Fragment> unfinished) {
        if (this.list.size() == 3) {
            return "<" + list.get(1).generateFullExpression() + ">";
        } else {
            return "";
        }
    }

    @Override
    public String generateFullExpression() {
        if (this.list.size() == 3) {
            return "<" + list.get(1).generateFullExpression() + ">";
        } else {
            return "";
        }
    }

    @Override
    public String getValue() {
        return list.get(1).generateFullExpression();
    }

    public URIFragment() {
        this.addFragment(new KeyFragment("<"));
    }

    public boolean isComplete() {
        // URI consists out of beginning delimiter "<", some value and ending delimiter ">"
        return list.size() == 3;
    }
}
