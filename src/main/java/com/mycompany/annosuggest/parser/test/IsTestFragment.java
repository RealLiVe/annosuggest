package com.mycompany.annosuggest.parser.test;


import com.mycompany.annosuggest.parser.Fragment;
import com.mycompany.annosuggest.parser.KeyFragment;
import com.mycompany.annosuggest.parser.URIFragment;
import com.mycompany.annosuggest.parser.ValueFragment;
import com.mycompany.annosuggest.parser.interfaces.SpecificTestFragment;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IsTestFragment extends Fragment implements SpecificTestFragment {

    protected String keyword = "is";

    public IsTestFragment(Fragment x) {
        this.list.add(x);
    }

    @Override
    public Fragment parse(StringReader in) throws IOException {

        skipWhiteSpaces(in);

        // Breakpoints
        Pattern tokens = Pattern.compile(Pattern.quote("<") + "|"
                + Pattern.quote(" "));

        String ldPath = this.readUntilEnd(in);
        Matcher matcher = tokens.matcher(ldPath);

        if (matcher.find()) {

            String delimiter = ldPath.substring(matcher.start(), matcher.end());
            String fragStr = ldPath.substring(0, matcher.start());

            // skip already readed chars at the reader.
            in.skip(matcher.end());

            switch (delimiter) {
                case " ":
                    addKeyFragment(fragStr);
                    skipWhiteSpaces(in);
                    int data = in.read();
                    if (data != -1) {
                        // contains data
                        char cdata = (char) data;
                        switch (cdata) {
                            case '<':
                                this.addFragment(new URIFragment().parse(in));
                                break;
                            case '"':
                                this.addFragment(new ValueFragment("\"" + this.findEndOfCurrentText(in)));
                                break;
                            default:
                                in.skip(-1);
                                ldPath = this.readUntilEnd(in);
                                Pattern tokens2 = Pattern.compile(Pattern.quote("&") + "|"
                                        + Pattern.quote("|") + "|"
                                        + Pattern.quote(" ") + "|"
                                        + Pattern.quote("]"));
                                Matcher matcher2 = tokens2.matcher(ldPath);

                                if (matcher2.find()) {
                                    in.skip(matcher2.end());
                                    fragStr = ldPath.substring(0, matcher.start());
                                    this.addFragment(new ValueFragment(fragStr));
                                    in.skip(-1);
                                } else {
                                    this.addFragment(new UnknownTestValueFragment(ldPath));
                                    in.skip(ldPath.length());
                                }
                                break;
                        }
                    }
                    return this;
                case "<":
                    addKeyFragment(fragStr);
                    this.addFragment(new URIFragment().parse(in));
                    return this;
                default:
                    this.syntaxError(delimiter);
            }


        } else {
            in.skip(ldPath.length());
            ldPath = ldPath.trim();
            if (!ldPath.isEmpty()) {
                if (this.keyword.toLowerCase().startsWith(ldPath.trim())) {
                    this.addFragment(new UnknownTestValueFragment(ldPath));
                } else {
                    this.syntaxError(ldPath);
                }
            }
        }
        return this;
    }

    protected void addKeyFragment(String key) {
        if (this.keyword.equalsIgnoreCase(key.trim())) {
            this.addFragment(new KeyFragment(key));
        } else {
            syntaxError(key);
        }
    }

    @Override
    public String generateExpression(List<Fragment> unfinished) {
        if (list.size() == 1) {
            unfinished.add(list.getLast());
        } else {
            unfinished.add(this);
        }
        return "";
    }

    @Override
    public String generateFullExpression() {
        StringBuilder b = new StringBuilder();
        if (list.size() == 3) {
            b.append(list.get(0).generateFullExpression());
            b.append(' ');
            b.append(list.get(1).generateFullExpression());
            b.append(' ');
            b.append(list.get(2).generateFullExpression());
        }
        return b.toString();
    }

}