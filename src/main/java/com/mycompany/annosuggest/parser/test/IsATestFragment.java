package com.mycompany.annosuggest.parser.test;

import com.mycompany.annosuggest.parser.Fragment;
import com.mycompany.annosuggest.parser.KeyFragment;
import com.mycompany.annosuggest.parser.URIFragment;
import com.mycompany.annosuggest.parser.ValueFragment;
import com.mycompany.annosuggest.parser.interfaces.SpecificTestFragment;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RealLiVe on 25.01.2016.
 */
public class IsATestFragment extends Fragment implements SpecificTestFragment {


    public IsATestFragment() {
        this.addFragment(new KeyFragment("is-a"));
    }

    @Override
    public Fragment parse(StringReader in) throws IOException {

        skipWhiteSpaces(in);

        int cdata = in.read();
        if (cdata == -1) {
            return this;
        }

        char chData = (char) cdata;
        if (chData == '<') {
            this.addFragment(new URIFragment().parse(in));
            return this;
        } else {
            in.skip(-1);
        }

        skipWhiteSpaces(in);

        // Breakpoints
        Pattern tokens = Pattern.compile(
                Pattern.quote("<") + "|" +
                        Pattern.quote(" ") + "|" +
                        Pattern.quote("|") + "|" +
                        Pattern.quote("&") + "|" +
                        Pattern.quote("]"));

        String ldPath = this.readUntilEnd(in);
        Matcher matcher = tokens.matcher(ldPath);

        if (matcher.find()) {
            String delimiter = ldPath.substring(matcher.start(), matcher.end());
            String fragStr = ldPath.substring(0, matcher.start());

            if (delimiter.equals("<")) {
                this.addFragment(new URIFragment().parse(in));
            } else {
                // refresh matcher
                in.skip(matcher.end());
                in.skip(-1);
                skipWhiteSpaces(in);
                if (!fragStr.isEmpty()) {
                    this.addFragment(new ValueFragment(fragStr));
                }
            }

            return this;
        } else {
            in.skip(ldPath.length());
            this.addFragment(new UnknownTestValueFragment(ldPath));
            return this;
        }
    }

    @Override
    public String generateExpression(List<Fragment> unfinished) {
        unfinished.add(this);
        return "";
    }

    @Override
    public String generateFullExpression() {
        StringBuilder b = new StringBuilder();
        if (list.size() == 2) {
            b.append(list.get(0).generateFullExpression());
            b.append(' ');
            b.append(list.get(1).generateFullExpression());
        }
        return b.toString();
    }

}
