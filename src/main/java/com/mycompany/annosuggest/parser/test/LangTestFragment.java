package com.mycompany.annosuggest.parser.test;


import com.mycompany.annosuggest.parser.Fragment;
import com.mycompany.annosuggest.parser.ValueFragment;
import com.mycompany.annosuggest.parser.interfaces.SpecificTestFragment;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RealLiVe on 25.01.2016.
 */
public class LangTestFragment extends Fragment implements SpecificTestFragment {

    @Override
    public Fragment parse(StringReader in) throws IOException {
        // Breakpoints
        Pattern tokens = Pattern.compile((
                Pattern.quote(" ") + "|"
                        + Pattern.quote("]") + "|")
                + Pattern.quote("&") + "|"
                + Pattern.quote("|")
        );

        String ldPath = this.readUntilEnd(in);
        Matcher matcher = tokens.matcher(ldPath);

        if (matcher.find()) {
            String delimiter = ldPath.substring(matcher.start(), matcher.end());
            String fragStr = ldPath.substring(0, matcher.start());

            // refresh reader
            in.skip(matcher.end());

            switch (delimiter) {
                case " ":
                    this.addFragment(new ValueFragment(fragStr));
                    return this;
                case "]":
                case "&":
                case "|":
                    in.skip(-1);
                    this.addFragment(new ValueFragment(fragStr));
                    return this;
                default:
                    this.syntaxError(delimiter);
            }
        } else {
            if (ldPath != null) {
                in.skip(ldPath.length());
                ldPath = ldPath.trim();
                if (!ldPath.isEmpty()) {
                    this.addFragment(new UnknownTestValueFragment(ldPath));
                }
            }
        }
        return this;
    }

    @Override
    public String generateExpression(List<Fragment> unfinished) {
        unfinished.add(this);
        return "";
    }

    @Override
    public String generateFullExpression() {
        StringBuilder b = new StringBuilder();
        if (!list.isEmpty()) {
            b.append('@');
            b.append(list.get(0).generateFullExpression());
        }
        return b.toString();
    }
}
