package com.mycompany.annosuggest.parser.test;

import com.mycompany.annosuggest.parser.UnknownValueFragment;

/**
 * Incomplete or undetected value in a test.
 */
public class UnknownTestValueFragment extends UnknownValueFragment {
    public UnknownTestValueFragment(String value) {
        super(value);
    }
}
