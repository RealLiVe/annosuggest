package com.mycompany.annosuggest.parser.test;


import com.mycompany.annosuggest.parser.Fragment;
import com.mycompany.annosuggest.parser.KeyFragment;
import com.mycompany.annosuggest.parser.URIFragment;
import com.mycompany.annosuggest.parser.ValueFragment;
import com.mycompany.annosuggest.parser.interfaces.SpecificTestFragment;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents a collection of tests.
 */
public class TestFragment extends Fragment {

    @Override
    public Fragment parse(StringReader in) throws IOException {
        // Breakpoints
        Pattern tokens = Pattern.compile(Pattern.quote("<") + "|"
                + Pattern.quote(" ") + "|"
                + Pattern.quote("&") + "|"
                + Pattern.quote("|") + "|"
                + Pattern.quote("]") + "|"
                + Pattern.quote("[") + "|"
                + Pattern.quote("@"));

        String ldPath = this.readUntilEnd(in);
        Matcher matcher = tokens.matcher(ldPath);

        if (matcher.find()) {
            do {
                String delimiter = ldPath.substring(matcher.start(), matcher.end());
                String fragStr = ldPath.substring(0, matcher.start());

                // skip already readed chars at the reader.
                in.skip(matcher.end());

                switch (delimiter) {
                    case "<":
                        if (fragStr.trim().isEmpty()) {
                            Fragment temp = new URIFragment().parse(in);
                            if (isDoubleValTest(in)) {
                                this.addFragment(new IsTestFragment(temp).parse(in));
                            } else {
                                this.addFragment(temp);
                            }
                        } else {
                            switch (fragStr.trim()) {
                                case "is-a":
                                    IsATestFragment tFrag = new IsATestFragment();
                                    tFrag.addFragment(new URIFragment().parse(in));
                                    this.addFragment(tFrag);
                                    break;
                                default:
                                    System.err.println("Syntax error. ");
                            }
                        }
                        break;
                    case " ":
                        if (!fragStr.trim().isEmpty()) {
                            switch (fragStr) {
                                case "is-a":
                                    this.addFragment(new IsATestFragment().parse(in));
                                    break;
                                default:
                                    if (isDoubleValTest(in)) {
                                        this.addFragment(new IsTestFragment(new ValueFragment(fragStr)).parse(in));
                                    } else {
                                        this.addFragment(new ValueFragment(fragStr));
                                    }
                            }
                        } else {
                            skipWhiteSpaces(in);
                        }
                        break;
                    case "&":
                    case "|":
                        this.addFragment(new KeyFragment(delimiter));
                        break;
                    case "]":
                        if (fragStr != null && !fragStr.isEmpty()) {
                            this.addFragment(new ValueFragment(fragStr));
                        }
                        return this;
                    case "@":
                        this.addFragment(new LangTestFragment().parse(in));
                        break;
                    default:
                        this.syntaxError(delimiter);
                }

                // refresh string representation of ldPath and matcher
                ldPath = this.readUntilEnd(in);
                matcher = tokens.matcher(ldPath);

            } while (matcher.find());

        }

        if (ldPath != null) {
            in.skip(ldPath.length());
            ldPath = ldPath.trim();
            if (!ldPath.isEmpty()) {
                this.addFragment(new UnknownTestValueFragment(ldPath));
            }
        }

        return this;
    }

    private boolean isDoubleValTest(StringReader in) {
        // Breakpoints
        Pattern tokens = Pattern.compile(Pattern.quote("&") + "|"
                + Pattern.quote("|") + "|"
                + Pattern.quote("]"));

        String ldPath = this.readUntilEnd(in);
        Matcher matcher = tokens.matcher(ldPath);
        if (matcher.find()) {
            String fragStr = ldPath.substring(0, matcher.start());
            return !fragStr.trim().isEmpty();
        }
        return true;
    }

    @Override
    public String generateExpression(List<Fragment> unfinished) {
        if (!list.isEmpty()) {
            if (list.getLast() instanceof SpecificTestFragment) {
                // should handle it themselves.
                list.getLast().generateExpression(unfinished);
            } else {
                unfinished.add(list.getLast());
            }
        } else {
            unfinished.add(new UnknownTestValueFragment(""));
        }
        return "";
    }

    @Override
    public String generateFullExpression() {
        StringBuilder b = new StringBuilder();
        b.append('[');
        for (Fragment x : list) {
            if (x instanceof KeyFragment) {
                // more pretty
                b.append(' ');
                b.append(x.generateFullExpression());
                b.append(' ');
            } else {
                b.append(x.generateFullExpression());
            }
        }
        b.append(']');
        return b.toString();
    }

    @Override
    public void addFragment(Fragment x) {
        if (list.isEmpty()) {
            if (x instanceof KeyFragment) {
                syntaxError(x);
            }
        } else {
            if (list.getLast() instanceof KeyFragment) {
                if (!(x instanceof SpecificTestFragment)
                        && !(x instanceof UnknownTestValueFragment)
                        && !(x instanceof ValueFragment)) {

                    syntaxError(x);
                }
            }
        }
        super.addFragment(x);
    }
}
