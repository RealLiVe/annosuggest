package com.mycompany.annosuggest.parser.path;


import com.mycompany.annosuggest.parser.*;
import com.mycompany.annosuggest.parser.interfaces.LeafFragment;
import com.mycompany.annosuggest.parser.test.TestFragment;

import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Describes a path.
 */
public class PathFragment extends Fragment {

    private static final Logger log = Logger.getLogger(PathFragment.class.getName());

    @Override
    public Fragment parse(StringReader in) throws IOException {
        // Breakpoints
        Pattern tokens = Pattern.compile((
                Pattern.quote("|") + "|"
                        + Pattern.quote("(") + "|"
                        + Pattern.quote("&") + "|"
                        + Pattern.quote("/") + "|"
                        + Pattern.quote("[") + "|"
                        + Pattern.quote("^") + "|"
                        + Pattern.quote("<") + "|"
                        + Pattern.quote(")") + "|"
                        + Pattern.quote("]"))
        );

        String ldPath = this.readUntilEnd(in);
        Matcher matcher = tokens.matcher(ldPath);
        if (matcher.find()) {
            do {
                // read delimiter and segment before delimiter.
                String delimiter = ldPath.substring(matcher.start(), matcher.end());
                String fragStr = ldPath.substring(0, matcher.start());
                // refresh matcher
                in.skip(matcher.end());

                if (fragStr != null && !fragStr.trim().isEmpty()) {
                    // something before a delimiter detected.
                    this.addFragment(new ValueFragment(fragStr));
                }

                switch (delimiter) {
                    case "^":
                        ReverseFragment rev = new ReverseFragment();
                        this.addFragment(rev.parse(in));
                        break;
                    case "|":
                    case "&":
                        AlternativePathFragment ret = new AlternativePathFragment();
                        ret.addFragment(this);
                        ret.addFragment(new KeyFragment(delimiter));
                        return ret.parse(in);
                    case "(":
                        this.addFragment(new GroupFragment().parse(in));
                        // Group
                        break;
                    case "/":
                        this.addFragment(new KeyFragment(delimiter));
                        break;
                    case ")":
                        // should be handled by group.
                        return this;
                    case "<":
                        this.addFragment(new URIFragment().parse(in));
                        break;
                    case "[":
                        /* test segment detected. defer parse to test,
                        then go on when it is finished. */
                        this.addFragment(new TestFragment().parse(in));
                        break;
                    default:
                        // delimiter not allowed in this fragment.
                        this.syntaxError(delimiter);
                }

                ldPath = this.readUntilEnd(in);
                matcher = tokens.matcher(ldPath);

            } while (matcher.find());

        }

        if (ldPath != null && !ldPath.trim().isEmpty()) {
            in.skip(ldPath.length());
            this.addFragment(new UnknownValueFragment(ldPath));
        }

        return this;
    }

    @Override
    public String generateExpression(List<Fragment> unfinished) {
        List<Fragment> buffer = new LinkedList<>();
        StringBuilder b = new StringBuilder();
        if (list.size() > 0) {
            // temporary remove last item, special treatment required.
            Fragment last = list.removeLast();
            for (Fragment x : list) {
                if (x instanceof KeyFragment) {
                    buffer.add(x);
                } else {
                    for (Fragment l : buffer) {
                        b.append(l.generateFullExpression());
                    }
                    buffer = new LinkedList<>();
                    b.append(x.generateFullExpression());
                }
            }
            list.add(last);
            if (last instanceof LeafFragment) {
                unfinished.add(last);
            } else {
                b.append(last.generateExpression(unfinished));
            }
        } else {
            unfinished.add(this);
        }
        return b.toString();
    }

    @Override
    public String generateFullExpression() {
        StringBuilder b = new StringBuilder();
        for (Fragment x : list) {
            b.append(x.generateFullExpression());
        }
        return b.toString();
    }

    @Override
    public void addFragment(Fragment x) {
        if (!list.isEmpty()) {
            if (list.getLast() instanceof URIFragment || list.getLast() instanceof ValueFragment) {
                if (!(x instanceof TestFragment) && !(x instanceof KeyFragment)) {
                    syntaxError(x);
                }
            } else if (list.getLast() instanceof KeyFragment && x instanceof KeyFragment) {
                syntaxError(x);
            } else if (list.getLast() instanceof GroupFragment && !(x instanceof KeyFragment)) {
                syntaxError(x);
            }
        } else {
            if (x instanceof KeyFragment) {
                syntaxError(x);
            }
        }
        super.addFragment(x);
    }
}
