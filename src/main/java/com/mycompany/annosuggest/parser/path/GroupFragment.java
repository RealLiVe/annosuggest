package com.mycompany.annosuggest.parser.path;


import com.mycompany.annosuggest.parser.Fragment;
import com.mycompany.annosuggest.parser.KeyFragment;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Describes a group
 */
public class GroupFragment extends Fragment {
    @Override
    public Fragment parse(StringReader in) throws IOException {
        String ldPath = readUntilEnd(in).trim();

        list.add(new PathFragment().parse(in));

        // check if recursive
        String temp = this.readUntilEnd(in).trim();
        if (!temp.isEmpty()) {
            char x = temp.charAt(0);
            if (x == '*' || x == '+' || x == '{') {
                parseRecursive(in);
            }
        }
        return this;
    }

    @Override
    public String generateExpression(List<Fragment> unfinished) {
        StringBuilder b = new StringBuilder();
        for (Fragment x : list) {
            if (x instanceof KeyFragment) {
                // ignore - could only be recursive
            } else {
                b.append(x.generateExpression(unfinished));
            }
        }
        return b.toString();
    }

    @Override
    public String generateFullExpression() {
        StringBuilder b = new StringBuilder();
        if (!list.isEmpty()) {
            b.append('(');
            if (!(list.getFirst() instanceof KeyFragment)) {
                b.append(list.getFirst().generateFullExpression());
            }
            b.append(')');
            if (list.size() == 2) {
                b.append(list.get(1).generateFullExpression());
            }
        }
        return b.toString();
    }

    private void parseRecursive(StringReader in) throws IOException {
        String ldPath = this.readUntilEnd(in);

        // Breakpoints (need to add {n} etc. )
        Pattern tokens = Pattern.compile((
                Pattern.quote("*") + "|"
                        + Pattern.quote("+") + "|"
                        + Pattern.quote("{"))
        );

        Matcher matcher = tokens.matcher(ldPath);
        if (matcher.find()) {
            String delimiter = ldPath.substring(matcher.start(), matcher.end());
            String fragStr = ldPath.substring(0, matcher.start());

            // refresh matcher
            in.skip(matcher.end());

            switch (delimiter) {
                case "*":
                case "+":
                    list.add(new KeyFragment(delimiter));
                    break;
                case "{":
                    parseSpecificRecursive(in);
                    break;
                default:
                    this.syntaxError(delimiter);
            }
        }
    }

    private void parseSpecificRecursive(StringReader in) throws IOException {
        String ldPath = this.readUntilEnd(in);

        // Breakpoints (need to add {n} etc. )
        Pattern tokens = Pattern.compile((Pattern.quote("}")));
        Matcher matcher = tokens.matcher(ldPath);

        if (matcher.find()) {
            String fragStr = ldPath.substring(0, matcher.start());
            in.skip(matcher.end());
            list.add(new KeyFragment("{" + fragStr + "}"));
        } else {
            // ignore.
        }
    }
}
