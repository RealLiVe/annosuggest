package com.mycompany.annosuggest.parser.path;


import com.mycompany.annosuggest.parser.Fragment;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

/**
 * Describes alternative paths with intersect or union operation
 */
public class AlternativePathFragment extends Fragment {

    @Override
    public Fragment parse(StringReader in) throws IOException {
        this.addFragment(new PathFragment().parse(in));
        return this;
    }

    @Override
    public String generateExpression(List<Fragment> unfinished) {
        if (list.getLast() instanceof PathFragment || list.getLast() instanceof GroupFragment) {
            return list.getLast().generateExpression(unfinished);
        } else {
            return "";
        }
    }

    @Override
    public String generateFullExpression() {
        StringBuilder b = new StringBuilder();
        for (Fragment x : list) {
            b.append(x.generateFullExpression());
        }
        return b.toString();
    }
}
