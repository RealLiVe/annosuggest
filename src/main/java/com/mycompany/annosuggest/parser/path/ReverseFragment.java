package com.mycompany.annosuggest.parser.path;

import com.mycompany.annosuggest.parser.*;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RealLiVe on 18.02.2016.
 */
public class ReverseFragment extends Fragment {

    @Override
    public Fragment parse(StringReader in) throws IOException {
        this.list.add(new KeyFragment("^"));
        // Breakpoints
        Pattern tokens = Pattern.compile((
                Pattern.quote("|") + "|"
                        + Pattern.quote("&") + "|"
                        + Pattern.quote("/") + "|"
                        + Pattern.quote("[") + "|"
                        + Pattern.quote("<") + "|"
                        + Pattern.quote(")"))
        );

        String ldPath = this.readUntilEnd(in);
        Matcher matcher = tokens.matcher(ldPath);
        if (matcher.find()) {
            String delimiter = ldPath.substring(matcher.start(), matcher.end());
            String fragStr = ldPath.substring(0, matcher.start());
            // refresh matcher
            in.skip(matcher.end());

            if (fragStr != null && !fragStr.trim().isEmpty()) {
                this.addFragment(new ValueFragment(fragStr));
            }

            switch (delimiter) {
                case "|":
                case "&":
                case "/":
                case ")":
                case "[":
                    in.skip(-1);
                    return this;
                case "<":
                    this.addFragment(new URIFragment().parse(in));
                    return this;
                default:
                    this.syntaxError(delimiter);
            }
        }

        if (ldPath != null && !ldPath.isEmpty()) {
            in.skip(ldPath.length());
            this.addFragment(new UnknownValueFragment(ldPath));
        }

        return this;
    }

    @Override
    public String generateExpression(List<Fragment> unfinished) {
        unfinished.add(this);
        return "";
    }

    @Override
    public String generateFullExpression() {
        StringBuilder b = new StringBuilder();
        for (Fragment x : list) {
            b.append(x.generateFullExpression());
        }
        return b.toString();
    }

}
