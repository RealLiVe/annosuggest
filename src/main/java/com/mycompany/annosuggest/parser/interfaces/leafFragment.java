package com.mycompany.annosuggest.parser.interfaces;

/**
 * Describes objects which are always leafs in syntax tree.
 */
public interface LeafFragment {
    public String getValue();
    //public boolean isFinished();
}
