package com.mycompany.annosuggest.parser;

/**
 * Signals, that the Parser could not parse given string.
 */
public class ParseException extends Exception {

    /**
     * Empty default constructor.
     */
    public ParseException() {
        super();
    }

    /**
     * Constructor with custom error message.
     *
     * @param errorMessage The error message.
     */
    public ParseException(String errorMessage) {
        super(errorMessage);
    }

    /**
     * Constructor with custom error message and previous exception.
     *
     * @param errorMessage The error The error message.
     * @param e            The previous exception.
     */
    public ParseException(String errorMessage, Exception e) {
        super(errorMessage, e);
    }

}
