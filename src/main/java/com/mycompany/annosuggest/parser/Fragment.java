package com.mycompany.annosuggest.parser;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;

/**
 * A Fragment describes a Node in the Tree-Structure of the LDPath-Parser. Every structure in the parser should inherit
 * from this class.
 */
public abstract class Fragment {

    protected LinkedList<Fragment> list = new LinkedList<>();

    /**
     * Adds a Fragment to this node.
     *
     * @param item Node to add.
     */
    public void addFragment(Fragment item) {
        list.add(item);
    }

    /**
     * Gets the list of fragments assigned to this node.
     *
     * @return the reference to the list. Changes will affect the node structure.
     */
    public LinkedList<Fragment> getFragment() {
        return list;
    }

    /**
     * Parse-Function. Used to recognize and add fragments out of a given LDPath.
     * <p>
     * To implement own: Search for markers, defer parsing to the recognized elements, add to list.
     *
     * @param in Reader which contains LDPath
     * @return The Fragment.
     * @throws IOException if something bad happens.
     */
    public abstract Fragment parse(StringReader in) throws IOException;

    /**
     * Helper function. Should read everything out of the Reader, without changing its position.
     *
     * @param in The reader to read from.
     * @return the String
     */
    protected String readUntilEnd(StringReader in) {
        StringBuilder b = new StringBuilder();
        try {
            in.mark(0);
            int data = in.read();
            while (data != -1) {
                //do something with data...
                b.append((char) data);
                data = in.read();
            }
            in.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return b.toString();
    }

    /**
     * Helper function. Skips beginning whitespaces.
     *
     * @param in The reader to read from.
     * @throws IOException if something happens with the reader
     */
    protected void skipWhiteSpaces(Reader in) throws IOException {
        in.mark(0);
        int data = -1;
        while ((data = in.read()) != -1) {
            char dataChar = (char) data;
            if (dataChar != ' ') {
                in.reset();
                return;
            } else {
                in.mark(0);
            }
        }
    }

    /**
     * Helper function. Searches the end of an escaped string. Usually needed in String-Literals
     *
     * @param in The reader to read from.
     * @return The string.
     * @throws IOException if something happens with the reader.
     */
    protected String findEndOfCurrentText(Reader in) throws IOException {
        boolean escaped = false;
        StringBuilder b = new StringBuilder();
        for (int x = in.read(); x != -1; x = in.read()) {
            char current = (char) x;
            switch (current) {
                case '\\':
                    b.append(current);
                    // escape sequence. if character is escaped, then it does not abort current sequence.
                    escaped = !escaped;
                    break;
                case '"':
                    // may be ending character.
                    if (escaped) {
                        // ... but it is not. character was escaped.
                        escaped = false;
                        b.append(current);
                    } else {
                        b.append(current);
                        // .. and it is one. job fulfilled.
                        return b.toString();
                    }
                    break;
                default:
                    b.append(current);
            }
        }
        return b.toString();
    }

    /**
     * Used to generate a valid LDPath-Expression. Completed elements should be added to the String,
     * potentially unfinished elements in the list.
     *
     * @param unfinished list to track unfinished elements.
     * @return (hopefully) valid ldpath-expression.
     */
    public abstract String generateExpression(List<Fragment> unfinished);

    /**
     * This method is only called internally. It usually is used to device the problem of generateExpression.
     * Usually, only the last path is unfinished, the rest should be well.
     *
     * @return hopefully valid ldpath expression.
     */
    public abstract String generateFullExpression();

    /**
     * Helper method for throwing a syntax error.
     *
     * @param delimiter delimiter which caused it.
     */
    protected void syntaxError(String delimiter) {
        throw new IllegalStateException("Delimiter " + delimiter + " not allowed in current state of " + this.getClass().getSimpleName());
    }

    /**
     * Helper method for throwing a syntax error.
     *
     * @param current fragment which caused it.
     */
    protected void syntaxError(Fragment current) {
        if (list.isEmpty()) {
            throw new IllegalStateException("Fragment " + current.getClass()
                    + " not allowed at begin of " + this.getClass().getSimpleName());
        } else {
            throw new IllegalStateException("Fragment " + current.getClass()
                    + " not allowed after " + list.getLast() + " in " + this.getClass().getSimpleName());
        }
    }
}

