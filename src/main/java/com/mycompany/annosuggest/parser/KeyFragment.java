package com.mycompany.annosuggest.parser;

import com.mycompany.annosuggest.parser.interfaces.LeafFragment;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

/**
 * Fragment which contains strings with special meanings. Usually Keywords or delimiters.
 */
public class KeyFragment extends Fragment implements LeafFragment {

    String value = "";

    @Override
    public Fragment parse(StringReader in) throws IOException {
        value = in.toString();
        return this;
    }

    @Override
    public String generateExpression(List<Fragment> unfinished) {
        return value;
    }

    @Override
    public String generateFullExpression() {
        return value;
    }

    public KeyFragment(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
