package com.mycompany.annosuggest.parser;

import com.mycompany.annosuggest.parser.interfaces.LeafFragment;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

/**
 * Contains a simple text value.
 */
public class ValueFragment extends Fragment implements LeafFragment {

    String value = "";

    @Override
    public Fragment parse(StringReader in) throws IOException {
        value = in.toString();
        return this;
    }

    @Override
    public String generateExpression(List<Fragment> unfinished) {
        return value;
    }

    @Override
    public String generateFullExpression() {
        return value;
    }

    public ValueFragment(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
