package com.mycompany.annosuggest.parser;

import com.mycompany.annosuggest.parser.path.PathFragment;

import java.io.IOException;
import java.io.StringReader;

/**
 * Main class for the Parser.
 */
public class Parser {

    /**
     * Constructor. Needed to construct a parser object.
     */
    public Parser() {

    }

    /**
     * Parses an LDPath-Expression. It does not need to be complete, the parser should recognize the last potentially
     * invalid node.
     *
     * @param ldPath the ldpath to parse
     * @return the parsed expression as tree.
     */
    public Fragment parse(String ldPath) throws ParseException {
        try {
            return new PathFragment().parse(new StringReader(ldPath));
        } catch (IOException | IllegalStateException e) {
            throw new ParseException(e.getMessage(), e);
        }
    }
}
