/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.annosuggest.io;

import java.util.LinkedList;

/**
 * The Result.
 */
public class ResultDTO {
    protected LinkedList<String> suggestion = new LinkedList<>();

    public ResultDTO() {
    }

    public LinkedList<String> getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(LinkedList<String> suggestion) {
        this.suggestion = suggestion;
    }

}
