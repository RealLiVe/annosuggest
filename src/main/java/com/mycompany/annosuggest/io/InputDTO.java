/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.annosuggest.io;

/**
 * The user input.
 */
public class InputDTO {

    protected String ldPath;

    public InputDTO(String ldPath) {
        this.ldPath = ldPath;
    }

    public InputDTO() {
    }

    public String getLdPath() {
        return ldPath;
    }

    public void setLdPath(String ldPath) {
        this.ldPath = ldPath;
    }

}
