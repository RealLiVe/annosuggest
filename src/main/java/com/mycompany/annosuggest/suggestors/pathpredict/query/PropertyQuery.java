package com.mycompany.annosuggest.suggestors.pathpredict.query;

import com.github.anno4j.querying.QueryService;
import com.mycompany.annosuggest.parser.Fragment;
import com.mycompany.annosuggest.parser.KeyFragment;
import com.mycompany.annosuggest.parser.URIFragment;
import com.mycompany.annosuggest.parser.UnknownValueFragment;
import com.mycompany.annosuggest.parser.interfaces.LeafFragment;
import com.mycompany.annosuggest.parser.path.PathFragment;
import com.mycompany.annosuggest.parser.path.ReverseFragment;
import com.mycompany.annosuggest.parser.test.UnknownTestValueFragment;
import org.apache.jena.atlas.logging.Log;
import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;

import java.util.LinkedList;

/**
 * Querying for predicates.
 */
public class PropertyQuery extends AbstractQuery {

    private boolean forward = true;
    private boolean findLiterals = true;
    private boolean findIRI = true;

    public void find(boolean literals, boolean iri) {
        this.findIRI = iri;
        this.findLiterals = literals;
    }

    public PropertyQuery(Fragment fragment, QueryService queryService) {
        this.ldPathParsed = fragment;
        this.queryService = queryService;
        LinkedList<Fragment> temp = new LinkedList<>();
        this.ldPath = ldPathParsed.generateExpression(temp);
        setTrackedFragment(temp.getLast());
    }

    @Override
    protected void processCurrentNode(Value v) throws RepositoryException {
        if (v instanceof Literal) {
            // cannot do anything.
            Log.info(v.toString(), "Literal found. No next hop. ");
        } else if (v instanceof URI) {
            Log.info(v.toString(), "URI");
            if (forward) {
                this.getNextHops((URI) v);
            } else {
                this.getPrevHops((URI) v);
            }
        }
    }

    /**
     * This method fetches next possible aims.
     *
     * @param uri The uri of the node you are currently working on.
     * @throws RepositoryException
     */
    private void getNextHops(URI uri) throws RepositoryException {
        RepositoryResult<Statement> result = connection.getStatements(uri, null, null);
        while (result.hasNext()) {
            Statement statement = result.next();
            URI predicate = statement.getPredicate();
            Value object = statement.getObject();
            if (object instanceof URI && findIRI || object instanceof Literal && findLiterals) {
                this.rank(uri, predicate, object);
            }
        }
    }

    /**
     * This method fetches next possible aims.
     *
     * @param uri The uri of the node you are currently working on.
     * @throws RepositoryException
     */
    private void getPrevHops(URI uri) throws RepositoryException {
        RepositoryResult<Statement> result = connection.getStatements(null, null, uri);

        while (result.hasNext()) {
            Statement statement = result.next();
            URI predicate = statement.getPredicate();
            Value object = statement.getSubject();

            if (object instanceof URI && findIRI || object instanceof Literal && findLiterals) {
                this.rank(uri, predicate, object);
            }
        }
    }

    private void rank(URI uri, URI predicate, Value aim) throws RepositoryException {
        if (trackedFragment instanceof LeafFragment) {
            // we search pref:name - convert the predicate to this form.
            String sRep = genNeededURI(predicate);

            // check if prefix is registered and current uri can be converted to this form. if yes, then cool. if not, then shit.
            if (sRep != null && sRep.startsWith(((LeafFragment) trackedFragment).getValue())) {

                // ranking: found one more occurrence of predicate.
                String key = predicate.getNamespace() + predicate.getLocalName();
                SuggestResult x = resultMap.getOrDefault(key, new SuggestResult(sRep));
                x.incrementOccurrences();

                if (aim instanceof URI) {
                    // object is an uri - use his incoming connections.
                    x.setMaxIncomingConnections(this.getIncomingPaths((URI) aim));
                } else if (aim instanceof Literal) {
                    // object is a literal.
                    // belongs to parent, use incoming connections of parent node.
                    x.setMaxIncomingConnections(this.getIncomingPaths(uri));
                }

                // update results.
                resultMap.put(key, x);
            }
        }
    }

    private String genNeededURI(URI result) {
        if (trackedFragment instanceof UnknownValueFragment) {
            return this.convertToPrefix(result);
        } else if (trackedFragment instanceof URIFragment) {
            return result.getNamespace() + result.getLocalName();
        }
        return null;
    }

    public void setTrackedFragment(Fragment x) {
        // reset
        forward = true;
        findLiterals = true;
        findIRI = true;

        if (x instanceof UnknownTestValueFragment) {
            forward = true;
            findIRI = false;
            findLiterals = true;
            trackedFragment = x;

        } else if (x instanceof URIFragment
                || x instanceof UnknownValueFragment) {

            if (x instanceof URIFragment) {
                if (((URIFragment) x).isComplete()) {
                    return;
                }
            }
            forward = true;
            trackedFragment = x;

        } else if (x instanceof ReverseFragment) {
            forward = false;
            LinkedList<Fragment> k = x.getFragment();
            trackedFragment = k.getLast();
            if (trackedFragment instanceof KeyFragment) {
                trackedFragment = new UnknownValueFragment("");
            }
        } else if (x instanceof KeyFragment) {
            if (((KeyFragment) x).getValue().equals("/")) {
                trackedFragment = new UnknownValueFragment("");
            }

        } else if (x instanceof PathFragment) {
            trackedFragment = new UnknownValueFragment("");
        }
    }


}
