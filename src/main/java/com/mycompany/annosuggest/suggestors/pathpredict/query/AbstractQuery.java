package com.mycompany.annosuggest.suggestors.pathpredict.query;

import com.github.anno4j.model.Annotation;
import com.github.anno4j.querying.QueryService;
import com.mycompany.annosuggest.parser.Fragment;
import org.apache.jena.atlas.logging.Log;
import org.apache.marmotta.ldpath.api.selectors.NodeSelector;
import org.apache.marmotta.ldpath.backend.sesame.SesameConnectionBackend;
import org.apache.marmotta.ldpath.backend.sesame.SesameValueBackend;
import org.apache.marmotta.ldpath.parser.LdPathParser;
import org.apache.marmotta.ldpath.parser.ParseException;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.object.ObjectConnection;

import java.io.StringReader;
import java.util.*;

/**
 * Querying of the suggestions and calculating of result.
 */
public abstract class AbstractQuery {
    protected String ldPath;
    protected Fragment ldPathParsed;
    protected Fragment trackedFragment;
    protected NodeSelector<Value> selector = null;
    protected SesameConnectionBackend connectionBackend;
    protected ObjectConnection connection;
    protected HashMap<String, SuggestResult> resultMap = new HashMap<>();
    protected HashMap<String, Integer> cache = new HashMap<>();
    protected QueryService queryService;

    protected void useConnection(ObjectConnection connection) {
        this.connection = connection;
        connectionBackend = new SesameConnectionBackend(connection);
    }

    protected String convertToPrefix(URI result) {
        for (Map.Entry<String, String> entry : queryService.getPrefixes().entrySet()) {
            if (entry.getValue().equals(result.getNamespace())) {
                return entry.getKey() + ":" + result.getLocalName();
            }
        }
        return null;
    }

    protected int getIncomingPaths(URI uri) throws RepositoryException {
        String key = uri.getNamespace() + uri.getLocalName();
        if (cache.containsKey(key)) {
            return cache.get(key);
        } else {
            int found = 0;
            RepositoryResult<Statement> result = connection.getStatements(null, null, uri);
            while (result.hasNext()) {
                ++found;
                result.next();
            }
            cache.put(key, found);
            return found;
        }
    }

    public Collection<SuggestResult> getResults() {
        ArrayList<SuggestResult> b = new ArrayList<>(resultMap.size());
        for (Map.Entry<String, SuggestResult> entry : resultMap.entrySet()) {
            b.add(entry.getValue());
        }
        b.sort(Comparator.reverseOrder());
        return b;
    }

    protected void processAnnotation(Annotation item) throws RepositoryException {
        // Apply LDPath to Annotation.
        Resource i = item.getResource();
        if (selector != null) {
            // hop to this node.
            Collection<Value> lastNodes = selector.select(connectionBackend, i, null, null);
            for (Value v : lastNodes) {
                processCurrentNode(v);
            }
        } else {
            processCurrentNode(i);
        }
    }

    public void execute() throws RepositoryException, QueryEvaluationException, MalformedQueryException, ParseException {
        if (trackedFragment == null) {
            return;
        }
        List<Annotation> list = null;
        if (ldPath == null || ldPath.isEmpty()) {
            list = queryService
                    .execute();
        } else {
            list = queryService
                    .addCriteria(ldPath)
                    .execute();
            selector = new LdPathParser<>(new SesameValueBackend(), new StringReader(ldPath))
                    .parseSelector(queryService.getPrefixes());
        }

        if (connection == null && list.size() > 0) {
            // no own connection, can use the one from queryService
            this.useConnection(list.get(0).getObjectConnection());
        }

        for (Annotation item : list) {
            Log.info("", "Process: " + item.getResourceAsString());
            processAnnotation(item);
        }
    }

    protected abstract void processCurrentNode(Value v) throws RepositoryException;
}