package com.mycompany.annosuggest.suggestors.pathpredict.query;

/**
 * One (temporary) result of the calculating process. Stores ranking factors and implements comparation-logic.
 */
public class SuggestResult implements Comparable<SuggestResult> {

    private String rep;
    private int incomingConnections = 0;
    private int occurrences = 0;

    @Override
    public int compareTo(SuggestResult o) {
        if (o.incomingConnections < this.incomingConnections) {
            return 1;
        }
        if (o.incomingConnections > this.incomingConnections) {
            return -1;
        }
        if (o.occurrences < this.occurrences) {
            return 1;
        }
        if (o.occurrences > this.occurrences) {
            return -1;
        }
        return 0;
    }

    public int setMaxIncomingConnections(int v) {
        incomingConnections = (v > incomingConnections ? v : incomingConnections);
        return incomingConnections;
    }

    public int incrementOccurrences() {
        return ++occurrences;
    }


    public int getIncomingConnections() {
        return incomingConnections;
    }

    public int getOccurrences() {
        return occurrences;
    }

    public SuggestResult(String rep) {
        this.rep = rep;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SuggestResult suggestResult = (SuggestResult) o;

        if (incomingConnections != suggestResult.incomingConnections) return false;
        if (occurrences != suggestResult.occurrences) return false;
        return rep != null ? rep.equals(suggestResult.rep) : suggestResult.rep == null;

    }

    @Override
    public int hashCode() {
        int result = rep != null ? rep.hashCode() : 0;
        result = 31 * result + incomingConnections;
        result = 31 * result + occurrences;
        return result;
    }

    public String getRep() {
        return rep;
    }
}
