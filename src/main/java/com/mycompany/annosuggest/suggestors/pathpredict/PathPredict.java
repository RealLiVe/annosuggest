package com.mycompany.annosuggest.suggestors.pathpredict;

import com.github.anno4j.querying.QueryService;
import com.mycompany.annosuggest.io.InputDTO;
import com.mycompany.annosuggest.io.ResultDTO;
import com.mycompany.annosuggest.parser.Fragment;
import com.mycompany.annosuggest.parser.Parser;
import com.mycompany.annosuggest.parser.interfaces.LeafFragment;
import com.mycompany.annosuggest.parser.interfaces.SpecificTestFragment;
import com.mycompany.annosuggest.parser.path.PathFragment;
import com.mycompany.annosuggest.parser.path.ReverseFragment;
import com.mycompany.annosuggest.suggestors.Anno4jContainer;
import com.mycompany.annosuggest.suggestors.Suggestor;
import com.mycompany.annosuggest.suggestors.exception.InputException;
import com.mycompany.annosuggest.suggestors.exception.SuggestorException;
import com.mycompany.annosuggest.suggestors.pathpredict.query.AbstractQuery;
import com.mycompany.annosuggest.suggestors.pathpredict.query.PropertyQuery;
import com.mycompany.annosuggest.suggestors.pathpredict.query.SuggestResult;
import com.mycompany.annosuggest.suggestors.pathpredict.query.ValueQuery;
import org.apache.marmotta.ldpath.parser.ParseException;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;


@RestController
/**
 * Suggestion-Strategy.
 */
public class PathPredict implements Suggestor {

    private static Logger log = Logger.getLogger(PathPredict.class.getName());
    @Autowired
    private Anno4jContainer anno4jContainer;

    public PathPredict() {

    }

    @RequestMapping("/input")
    public ResultDTO suggest(@RequestBody InputDTO input)
            throws SuggestorException, InputException {

        String ldPath = input.getLdPath();

        // parse input query.
        Fragment parsed = null;
        try {
            parsed = new Parser().parse(ldPath);
        } catch (com.mycompany.annosuggest.parser.ParseException e) {
            log.severe("First parser detected invalid LDPath");
            throw new InputException(e.getMessage());
        }

        // calculate segment for auto complete.
        LinkedList<Fragment> track = new LinkedList<>();
        parsed.generateExpression(track);

        QueryService queryService = anno4jContainer.createQueryService();

        // Prepare querying
        AbstractQuery query = null;
        if (track.isEmpty()) {
            // cannot do anything.
            log.severe("Could not detect last segment");
            throw new InputException("Syntax Error!");

        } else if (track.getLast() instanceof LeafFragment
                || track.getLast() instanceof ReverseFragment
                || track.getLast() instanceof PathFragment) {
            // Query predicates
            query = new PropertyQuery(parsed, queryService);

        } else if (track.getLast() instanceof SpecificTestFragment) {
            // Test detected.
            // Query values
            query = new ValueQuery(parsed, queryService);

        } else {
            throw new SuggestorException("No action for last segment defined.");
        }

        try {
            query.execute();
        } catch (RepositoryException | QueryEvaluationException | MalformedQueryException e) {
            // user may not have done something bad.
            log.log(Level.SEVERE, "Server failure", e);
            throw new SuggestorException("Unknown Error!", e);
        } catch (ParseException e) {
            throw new InputException("Syntax Error!", e);
        }

        Collection<SuggestResult> result = query.getResults();

        // prepare output
        ResultDTO res = new ResultDTO();
        for (SuggestResult entry : result) {
            res.getSuggestion().add(entry.getRep());
        }

        return res;
    }

    /**
     * Method for replace Anno4j-Container. Currently needed for testing only.
     *
     * @param anno4jContainer
     */
    public void setAnno4jContainer(Anno4jContainer anno4jContainer) {
        this.anno4jContainer = anno4jContainer;
    }

}
