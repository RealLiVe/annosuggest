package com.mycompany.annosuggest.suggestors.pathpredict.query;

import com.github.anno4j.querying.QueryService;
import com.mycompany.annosuggest.parser.Fragment;
import com.mycompany.annosuggest.parser.URIFragment;
import com.mycompany.annosuggest.parser.UnknownValueFragment;
import com.mycompany.annosuggest.parser.interfaces.LeafFragment;
import com.mycompany.annosuggest.parser.test.IsATestFragment;
import com.mycompany.annosuggest.parser.test.IsTestFragment;
import com.mycompany.annosuggest.parser.test.LangTestFragment;
import org.apache.marmotta.ldpath.api.selectors.NodeSelector;
import org.apache.marmotta.ldpath.backend.sesame.SesameValueBackend;
import org.apache.marmotta.ldpath.parser.LdPathParser;
import org.apache.marmotta.ldpath.parser.ParseException;
import org.openrdf.model.Literal;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.repository.RepositoryException;

import java.io.StringReader;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Querying for objects.
 */
public class ValueQuery extends AbstractQuery {

    public ValueQuery(Fragment x, QueryService queryService) {
        LinkedList<Fragment> temp = new LinkedList<>();
        this.ldPathParsed = x;
        this.ldPath = x.generateExpression(temp);
        this.trackedFragment = temp.getLast();
        this.queryService = queryService;
    }

    @Override
    protected void processCurrentNode(Value v) throws RepositoryException {
        if (trackedFragment instanceof LangTestFragment) {
            this.handleLangTest(v);
        } else if (trackedFragment instanceof IsTestFragment) {
            this.handleIsTest(v);
        } else if (trackedFragment instanceof IsATestFragment) {
            this.handleIsATest(v);
        } else {
            System.err.println("Not detected!");
        }
    }

    private void handleIsATest(Value v) {
        Fragment resultType = trackedFragment.getFragment().getLast();
        if (resultType instanceof URIFragment && ((URIFragment) resultType).isComplete()) {
            // no need to complete.
            return;
        }
        try {
            NodeSelector<Value> hopper = new LdPathParser<>(new SesameValueBackend(), new StringReader("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"))
                    .parseSelector(null);

            int parent = getIncomingPaths((URI) v);

            Collection<Value> types = hopper.select(connectionBackend, v, null, null);

            for (Value x : types) {
                if (x instanceof URI) {
                    String sIRI = genNeededURI(resultType, (URI) x);
                    if (sIRI != null && sIRI.startsWith(((LeafFragment) resultType).getValue())) {
                        SuggestResult result = resultMap.getOrDefault(sIRI, new SuggestResult(sIRI));
                        result.incrementOccurrences();
                        result.setMaxIncomingConnections(parent);
                        resultMap.put(sIRI, result);
                    } else {
                        // no hit.
                    }
                }
            }
        } catch (ParseException | RepositoryException e) {
            e.printStackTrace();
        }
    }

    private void handleIsTest(Value v) {
        if (trackedFragment.getFragment().size() == 3) {
            Fragment property = trackedFragment.getFragment().getFirst();
            Fragment needle = trackedFragment.getFragment().getLast();
            if (needle.generateFullExpression().isEmpty() || !needle.generateFullExpression().startsWith("\"")) {
                // only literals supported in this version.
                return;
            }
            try {
                NodeSelector<Value> hopper = new LdPathParser<>(new SesameValueBackend(), new StringReader(property.generateFullExpression()))
                        .parseSelector(queryService.getPrefixes());

                int parent = getIncomingPaths((URI) v);

                Collection<Value> values = hopper.select(connectionBackend, v, null, null);

                for (Value x : values) {
                    if (x instanceof Literal) {
                        String compare = ((Literal) x).getLabel();
                        if (compareLabel(needle.generateFullExpression(), compare)) {
                            SuggestResult result = resultMap.getOrDefault(((Literal) x).getLabel(), new SuggestResult(((Literal) x).getLabel()));
                            result.incrementOccurrences();
                            result.setMaxIncomingConnections(parent);
                            resultMap.put(((Literal) x).getLabel(), result);
                        }
                    }
                }
            } catch (ParseException | RepositoryException e) {
                e.printStackTrace();
            }
        } else if (trackedFragment.getFragment().size() == 2) {
            Fragment x = trackedFragment.getFragment().getLast();
            String sug = "is";
            if (sug.startsWith(x.generateFullExpression()) && !sug.equalsIgnoreCase(x.generateFullExpression())) {
                resultMap.put("is", new SuggestResult("is"));
            }
        }
    }

    private boolean compareLabel(String needle, String toothpick) {
        needle = needle.replace("\"", "");
        needle = needle.replace("\\", "");
        toothpick = toothpick.replace("\"", "");
        toothpick = toothpick.replace("\\", "");
        return toothpick.startsWith(needle);
    }

    private void handleLangTest(Value v) {
        if (v instanceof Literal) {
            Literal current = (Literal) v;
            if (current.getLanguage() != null) {
                // get requested language.
                String needle = trackedFragment.generateFullExpression();
                if (needle.isEmpty() || current.getLanguage().startsWith(needle)) {
                    SuggestResult result = resultMap.getOrDefault(current.getLanguage(),
                            new SuggestResult(current.getLanguage()));

                    result.incrementOccurrences();

                    resultMap.put(current.getLanguage(), result);
                }
            } else {
                // literal has no language.
            }
        } else {
            // iri cannot have language.
        }
    }

    private String genNeededURI(Fragment f, URI result) {
        if (f instanceof UnknownValueFragment) {
            return this.convertToPrefix(result);
        } else if (f instanceof URIFragment) {
            return result.getNamespace() + result.getLocalName();
        }
        return null;
    }


}
