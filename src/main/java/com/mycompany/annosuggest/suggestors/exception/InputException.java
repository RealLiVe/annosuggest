package com.mycompany.annosuggest.suggestors.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
/**
 * The Input was wrong, may be a syntax error.
 */
public class InputException extends Exception {
    /**
     * Empty default constructor.
     */
    public InputException() {
        super();
    }

    /**
     * Constructor with custom error message.
     *
     * @param errorMessage The error message.
     */
    public InputException(String errorMessage) {
        super(errorMessage);
    }

    /**
     * Constructor with custom error message and previous exception.
     *
     * @param errorMessage The error The error message.
     * @param e            The previous exception.
     */
    public InputException(String errorMessage, Exception e) {
        super(errorMessage, e);
    }

}
