package com.mycompany.annosuggest.suggestors.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
/**
 * Something was wrong.
 */
public class SuggestorException extends Exception {

    /**
     * Empty default constructor.
     */
    public SuggestorException() {
        super();
    }

    /**
     * Constructor with custom error message.
     *
     * @param errorMessage The error message.
     */
    public SuggestorException(String errorMessage) {
        super(errorMessage);
    }

    /**
     * Constructor with custom error message and previous exception.
     *
     * @param errorMessage The error The error message.
     * @param e            The previous exception.
     */
    public SuggestorException(String errorMessage, Exception e) {
        super(errorMessage, e);
    }

}
