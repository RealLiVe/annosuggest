package com.mycompany.annosuggest.suggestors;

import com.github.anno4j.Anno4j;
import com.github.anno4j.querying.QueryService;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.config.RepositoryConfigException;
import org.springframework.stereotype.Component;

/**
 * Spring Bean for sharing anno4j-instance.
 */
@Component
public class Anno4jContainer {
    private Anno4j anno4j;

    public Anno4jContainer() {
        try {
            this.anno4j = new Anno4j();
        } catch (RepositoryException | RepositoryConfigException e) {
            e.printStackTrace();
        }
    }

    public QueryService createQueryService() {
        QueryService q = anno4j.createQueryService();
        return q;
    }
}
