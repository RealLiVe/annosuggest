/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.annosuggest.suggestors;

import com.mycompany.annosuggest.io.InputDTO;
import com.mycompany.annosuggest.io.ResultDTO;
import com.mycompany.annosuggest.suggestors.exception.InputException;
import com.mycompany.annosuggest.suggestors.exception.SuggestorException;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Needs to be implemented by specific suggestion strategy.
 */
public interface Suggestor {
    /**
     * Auto complete suggestions based on a given LDPath
     *
     * @param input the ldpath, stored in the InputDTO
     * @return the Result, stored in the ResultDTO
     * @throws SuggestorException if something happened.
     * @throws InputException     if there is an error with the given ldpath.
     */
    ResultDTO suggest(@RequestBody InputDTO input) throws SuggestorException, InputException;
}
