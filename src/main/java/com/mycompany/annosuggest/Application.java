/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.annosuggest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * main class to start the spring server
 */
@SpringBootApplication
public class Application {

    /**
     * main method to start the application
     *
     * @param args not used
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
